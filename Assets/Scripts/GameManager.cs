﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public float turnDelay = .1f;
	public static GameManager instance;
	public BoardManager board;
	public int playerFoodPoints = 100;
	[HideInInspector]
	public bool isPlayerTurn = true;

	private GameObject levelImage;
	private Text levelText;
	private int levelDelay = 2;
	private int level = 1;
	private List<Enemy> enemies;
	private bool enemiesMoving;
	private bool doingSetup;

	// Use this for initialization
	IEnumerator MoveEnemies() {
		enemiesMoving = true;
		yield return new WaitForSeconds(turnDelay);
		if (enemies.Count == 0) {
			yield return new WaitForSeconds(turnDelay);
		}

		for(int i = 0; i < enemies.Count; i++) {
			enemies[i].MoveEnemy();
			yield return new WaitForSeconds(enemies[i].movingTime);
		}
		isPlayerTurn = true;
		enemiesMoving = false;
	}

	private void HideLevelImage() {
		levelImage.SetActive(false);
		doingSetup = false;

	}
	public void GameOver() {
		levelText.text = "You starved";
		levelImage.SetActive(true);
		enabled = false;
	}

	public void AddEnemies(Enemy script) {
		enemies.Add(script);
	}

	void Awake () {
		if(instance == null)
		{
			instance = this;
		}else if(instance != this)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
		board = GetComponent<BoardManager>();
		InitGame();
	}

	void InitGame()
	{
		doingSetup = true;
		levelImage = GameObject.Find("LevelImage");
		levelText = GameObject.Find("LevelText").GetComponent<Text>();
		levelText.text = "Day " + level;
		levelImage.SetActive(true);
		Invoke("HideLevelImage", levelDelay);

		board.SetupScene(level);
		enemies = new List<Enemy>();
		level++;
	}
	
	// Update is called once per frame
	void Update () {
		if(enemiesMoving || isPlayerTurn || doingSetup) return;

		StartCoroutine(MoveEnemies());
		
	}

	void OnLevelWasLoaded(int index) {
		InitGame();
	}
}
