﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

	[Serializable]
	public class Count
	{
		public int minimum;
		public int maximum;

		public Count(int min,int max)
		{
			this.minimum = min;
			this.maximum = max;
		}
	}

	public int rows = 8;
	public int cols = 8;
	public Count wallCount = new Count(5, 9);
	public Count foodCount = new Count(5, 9);
	public GameObject exit;
	public GameObject[] floorTiles;
	public GameObject[] wallTiles;
	public GameObject[] foodTiles;
	public GameObject[] enemyTiles;
	public GameObject[] outerWallTiles;

	private Transform boardHolder;
	private List<Vector3> gridPositions = new List<Vector3>();

	private void InitializeList()
	{
		gridPositions.Clear();
		for (int i = 1; i < cols - 1; i++)
		{
			for (int j = 1; j < rows - 1; j++)
			{
				gridPositions.Add(new Vector3(i, j, 0f)); // inside grid
			}
		}
	}

	private void BoardSetup()
	{
		boardHolder = new GameObject("Board").transform;
		for (int i = -1; i < cols + 1; i++) // outside the board 
		{
			for (int j = -1; j < rows + 1; j++)
			{
				GameObject toInit = floorTiles[Random.Range(0, floorTiles.Length)];
				if (i == -1 || i == cols || j == -1 || j == rows) // edge of the board
				{
					toInit = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
				}
				GameObject instantiate = GameObject.Instantiate(toInit, new Vector3(i, j, 0f), Quaternion.identity) as GameObject;
				instantiate.transform.SetParent(boardHolder);
			}
		}
	}

	private Vector3 RandomPosition()
	{
		int randomIndex = Random.Range(0, gridPositions.Count);
		Vector3 randomPosition = gridPositions[randomIndex];
		gridPositions.RemoveAt(randomIndex); // remove index prevent from duplicate
		return randomPosition;
	}

	private void LayoutAtRandom(GameObject[] tileArray,Transform parent, int min,int max)
	{
		int objectCount = Random.Range(min, max + 1);
		for(int i = 0; i < objectCount; i++)
		{
			Vector3 randomPosition = RandomPosition();
			GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
			GameObject obj = Instantiate(tileChoice, randomPosition, Quaternion.identity) as GameObject;
			obj.transform.parent = parent;
		}
	}

	public void SetupScene(int level)
	{
		BoardSetup();
		InitializeList();
		LayoutAtRandom(wallTiles,new GameObject("Walls").transform, wallCount.minimum, wallCount.maximum);
		LayoutAtRandom(foodTiles, new GameObject("Foods").transform, foodCount.minimum, foodCount.maximum);
		int enemyCount = (int)Math.Log(level, 2);
		LayoutAtRandom(enemyTiles, new GameObject("Enemies").transform, enemyCount, enemyCount);
		Instantiate(exit, new Vector3(cols-1, rows-1), Quaternion.identity);
	}
}
