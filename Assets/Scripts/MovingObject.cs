﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {

	public float movingTime = .1f;
	public LayerMask blockingLayer;

	private BoxCollider2D boxCollider2D;
	private Rigidbody2D rigid2D;
	private float inverseMovingTime;


	// Use this for initialization
	protected virtual void Start () {
		boxCollider2D = GetComponent<BoxCollider2D>();
		rigid2D = GetComponent<Rigidbody2D>();
		inverseMovingTime = 1f / movingTime;
	}

	protected IEnumerator SmoothMovement(Vector3 end)
	{
		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
		while (sqrRemainingDistance > float.Epsilon)
		{
			Vector3 newPosition = Vector3.MoveTowards(rigid2D.position, end, inverseMovingTime * Time.deltaTime);
			rigid2D.MovePosition(newPosition);
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;
			yield return null;
		}
	}

	protected bool Move(int xDir,int yDir,out RaycastHit2D hit)
	{
		Vector2 start = transform.position;
		Vector2 end = start + new Vector2(xDir, yDir);

		boxCollider2D.enabled = false;
		hit = Physics2D.Linecast(start, end, blockingLayer);
		boxCollider2D.enabled = true;

		if(hit.transform == null)
		{
			StartCoroutine(SmoothMovement(end));
			return true;
		}
		return false;
	}

	protected virtual void AttemptMove <T> (int xDir,int yDir) where T :Component
	{
		RaycastHit2D hit;
		bool canMove = Move(xDir, yDir, out hit);
		if (hit.transform == null)
			return;
		T hitComponent = hit.transform.GetComponent<T>();
		if(!canMove && hitComponent != null)
		{
			OnCantMove(hitComponent);
		}
	}

	protected abstract void OnCantMove<T>(T component) where T : Component;
}
