﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointPerFood = 10;
	public int pointPerSoda = 20;
	public float restartLevelDelay = 1f;
	public Text foodText;

	public AudioClip moveSound1;
	public AudioClip moveSound2;
	public AudioClip eatSound1;
	public AudioClip eatSound2;
	public AudioClip drinkSound1;
	public AudioClip drinkSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip gameOverSound;

	private Animator animator;
	private int food;

	protected override void OnCantMove<T>(T component) {
		Wall hitWall = component as Wall;
		hitWall.DamageWall(wallDamage);
		animator.SetTrigger("PlayerChop");
		SoundManager.instance.PlayRandomSound(chopSound1, chopSound2);
	}

	protected override void AttemptMove<T>(int xDir, int yDir) {
		Food--;
		base.AttemptMove<T>(xDir, yDir);

		RaycastHit2D hit;
		if(Move(xDir,yDir,out hit)) {
			SoundManager.instance.PlayRandomSound(moveSound1,moveSound2);
		}

		CheckGameOver();
		GameManager.instance.isPlayerTurn = false;
	}

	private void CheckGameOver() {
		if(Food <= 0) {
			GameManager.instance.GameOver();
			SoundManager.instance.PlaySingleSound(gameOverSound);
			SoundManager.instance.musicSource.Stop();
		}
	}

	private void Restart() {
		Application.LoadLevel(Application.loadedLevel);
	}

	public void LoseFood(int loss) {
		animator.SetTrigger("PlayerHit");
		Food -= loss;
		CheckGameOver();
	}

	private int Food {
		get {
			return food;
		}
		set {
			food = value;
			foodText.text = "Food : " + food;
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Exit") {
			Invoke("Restart", restartLevelDelay);
			enabled = false;
		}else if(other.tag == "Food") {
			Food += pointPerFood;
			SoundManager.instance.PlayRandomSound(eatSound1, eatSound2);
			other.gameObject.SetActive(false);
		}else if(other.tag == "Soda") {
			Food += pointPerSoda;
			SoundManager.instance.PlayRandomSound(drinkSound1, drinkSound2);
			other.gameObject.SetActive(false);
		}
	}
	// Use this for initialization
	protected override void Start () {
		animator = GetComponent<Animator>();
		Food = GameManager.instance.playerFoodPoints;
		base.Start();
	}
	

	void OnDisable() {
		GameManager.instance.playerFoodPoints = Food;
	}


	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.isPlayerTurn) return;

		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw("Horizontal");
		vertical = (int)Input.GetAxisRaw("Vertical");


		if (horizontal != 0)
			vertical = 0;

		if(horizontal !=0 || vertical != 0) {
			AttemptMove<Wall>(horizontal, vertical);
		}
	}
}
