﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource efxSource;
	public AudioSource musicSource;
	public static SoundManager instance;

	private float minPitch = 0.95f;
	private float maxPitch = 1.05f;
	
	void Awake() {
		if(instance== null) {
			instance = this;
		}else if(instance != this) {
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	public void PlaySingleSound(AudioClip clip) {
		efxSource.clip = clip;
		efxSource.Play();
	}

	public void PlayRandomSound(params AudioClip[] clips) {
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(minPitch, maxPitch);
		efxSource.clip = clips[randomIndex];
		efxSource.pitch = randomPitch;
		efxSource.Play();
	}

}
